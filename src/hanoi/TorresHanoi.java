package hanoi;

import java.util.ArrayList;
import java.util.Stack;

import javax.swing.JOptionPane;

public class TorresHanoi{

	static ArrayList<Torre> torres = new ArrayList<Torre>();
	static final int CANTFICHAS = 10;
	static int cantJugadas = 0;
	public static void main(String[] args) {
		cargaInicial();
		try {
		while(!isWinner()) {
			mostrarTorres();
			int origen = Integer.parseInt(JOptionPane.showInputDialog("Ingrese Torre Origen"));
			int destino = Integer.parseInt(JOptionPane.showInputDialog("Ingrese Torre Destino"));
			if (!jugar(origen, destino)) {
				System.out.println("No puede mover de la Torre "+origen+" a la "+destino);
			}
			}
		mostrarResultado();
		}catch(Exception e) {
			System.out.println("Chau!");
		}
	}
	public static void cargaInicial() {
		Stack<Ficha> pila = new Stack<Ficha>();
		for(int i=CANTFICHAS;i>0;i--) {
			pila.push(new Ficha(i));	
		}
		
		torres.add(new Torre(pila));
		torres.add(new Torre(new Stack<Ficha>()));
		torres.add(new Torre(new Stack<Ficha>()));
	}
	public static void mostrarTorres() {
		for(int i=0;i<torres.size();i++) {
			if (torres.get(i).getFichas().size()>0)
				System.out.println("Torre: "+(i+1)+" Fichas: "+torres.get(i).getFichas().size()+" Size: "+torres.get(i).getFichas().peek().getTamanio());
			else
				System.out.println("Torre: "+(i+1)+" Fichas: "+torres.get(i).getFichas().size());
		}
	}
	
	public static boolean jugar(int origen, int destino) {
		cantJugadas++;
		if (torres.get(origen-1).getFichas().isEmpty())
			return false;
		Ficha ficha = torres.get(origen-1).sacar();
		if (!torres.get(destino-1).poner(ficha)) {
			torres.get(origen-1).poner(ficha);
			return false;
		}
		return true;
	}
	
	public static boolean isWinner() {
		if ((torres.get(1).getFichas().size()==CANTFICHAS)||(torres.get(2).getFichas().size()==CANTFICHAS)) {
			return true;
		}
		return false;
	}
	public static void mostrarResultado() {
		System.out.println("Cantidad de Jugadas: "+cantJugadas);
	}
}
