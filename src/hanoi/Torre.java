package hanoi;

import java.util.Stack;

public class Torre {
	private Stack<Ficha> fichas = new Stack<Ficha>();

	public Stack<Ficha> getFichas() {
		return fichas;
	}

	public void setFichas(Stack<Ficha> fichas) {
		this.fichas = fichas;
	}

	public Torre(Stack<Ficha> fichas) {
		super();
		this.fichas = fichas;
	}

	public Torre() {
	}
	public boolean poner(Ficha ficha) {
		if ((this.getFichas().isEmpty())||(this.getFichas().peek().getTamanio()>ficha.getTamanio())) {
			this.getFichas().push(ficha);
			return true;
		}else {
			return false;
		}
	}
	public Ficha sacar() {
		return this.getFichas().pop();
	}
}
